package com.java.javanumbersandstrings;

public class ConcatenateString {

	public static void main(String[] args) {
		
		String hi = "Hi, ";
		String mom = "mom.";
		
		
		// using concat function
		System.out.println(hi.concat(mom));
		
		// directly applying + operator
		System.out.println(hi+mom);

	}

}
