package com.java.javanumbersandstrings;

public class Adder {

	public static void main(String[] args) {
		
		int numberArg = args.length;

		//this program requires at least two arguments
	        if (numberArg < 2) {
	            System.out.println("This program requires two command-line arguments.");
	        } else {
		    int sum = 0;

		    for (int i = 0; i < numberArg; i++) {
	                sum += Integer.valueOf(args[i]).intValue();
		    }

		    //print the sum
	            System.out.println(sum);
	        }

	}

}
