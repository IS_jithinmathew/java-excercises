package com.java.javanumbersandstrings;

public class MinValues {

	public static void main(String[] args) {
		
        byte smallestByte = Byte.MIN_VALUE;
        short smallestShort = Short.MIN_VALUE;
        int smallestInteger = Integer.MIN_VALUE;
        long smallestLong = Long.MIN_VALUE;

        
        float smallestFloat = Float.MIN_VALUE;
        double smallestDouble = Double.MIN_VALUE;

       
        System.out.println("Smallest byte value is " + smallestByte);
        System.out.println("Smallest short value is " + smallestShort);
        System.out.println("Smallest integer value is " + smallestInteger);
        System.out.println("Smallest long value is " + smallestLong);

        System.out.println("Smallest float value is " + smallestFloat);
        System.out.println("Smallest double value is " + smallestDouble);

	}

}
