package com.java.collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Random {

	public static void main(String[] args) {
		 // shuffle the list of arguments
        List<String> argList = Arrays.asList(args);
        Collections.shuffle(argList);

       
        argList.stream()
        .forEach(e->System.out.format("%s ",e));

        // Print out the elements using for-each
        for (String arg: argList) {
            System.out.format("%s ", arg);
        }

        System.out.println();

	}

}
