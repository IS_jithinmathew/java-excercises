package com.java.javaclassesobjects;

public class NumberHolderMembers {

	public static void main(String[] args) {
		
		
		NumberHolder Obj = new NumberHolder();
		Obj.anInt = 18;
		Obj.aFloat = 8.6f;
		System.out.println(Obj.anInt);
		System.out.println(Obj.aFloat);

	}

}

// Created instance of the class NumberHolder and accessed its data members through its object
//and assigned some value and then displayed the value of each member variable.