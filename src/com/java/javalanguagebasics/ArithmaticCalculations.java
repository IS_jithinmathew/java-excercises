package com.java.javalanguagebasics;

public class ArithmaticCalculations {

	public static void main(String[] args) {
	
		// illegal declarations
		//int  1speed;
		
		
		ArithmaticCalculations obj=new ArithmaticCalculations();
		obj.carTest();
		
		 int i = 3;
	        i++;
	        System.out.println(i);    // "4"
	        ++i;                     
	        System.out.println(i);    // "5"
	        System.out.println(++i);  // "6"
	        System.out.println(i++);  // "6"
	        System.out.println(i);    // "7"
        
        //prefix operator, first will increment the value by 1 and print it
	        //postfix operator , first will take the actual value and then increment by1

	}
	
	public void carTest() {
		int b=4;
		System.out.println(b);

	}

}
